FROM --platform=$BUILDPLATFORM python:3.12-rc-slim-buster

LABEL Name="Python Flask Demo App" Version=1.4.2
LABEL org.opencontainers.image.source = "https://github.com/benc-uk/python-demoapp"

RUN apt-get update && apt-get install gcc -y && apt-get install git -y

ARG srcDir=src
WORKDIR /app

COPY $srcDir/run.py ./
COPY $srcDir/app ./app
COPY $srcDir/requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

CMD ["gunicorn", "-w", "2", "run:app", "--bind", "0.0.0.0:5000"]
